﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Delegate_Multicasting
{
    class Program
    {
        static void Main(string[] args)
        {
            StringOps so = new StringOps(); // Instance of a StringOps

            StrMod del_mod = so.replaceSpaces;
            string str;

            str = del_mod("Sala Gandu Naki?");
            Console.WriteLine("Resulting string: " + str);
            Console.WriteLine();

            del_mod = so.removeSpaces;
            str = del_mod("Sala Gandu Naki?");
            Console.WriteLine("Resulting string: " + str);
            Console.WriteLine();

            del_mod = so.reverse;
            str = del_mod("Sala Gandu Naki?");
            Console.WriteLine("Resulting string: " + str);
            Console.WriteLine();

            Console.ReadKey();
        }
    }

    delegate string StrMod(string str);

    /// <summary>
    /// This class contains some methods for trivial String manipulation.
    /// </summary>

    class StringOps
    {
        // Replaces spaces with hyphens.
        public string replaceSpaces(string a)
        {
            Console.WriteLine("Replaces spaces with hyphens.");
            return a.Replace(' ', '-');
        }

        // Remove spaces.
        public string removeSpaces(string a)
        {
            string temp = "";
            int i;
            Console.WriteLine("Removing spaces.");
            for (i = 0; i < a.Length; i++)
                if (a[i] != ' ') temp += a[i];
            return temp;
        }

        // Reverse a string.
        public string reverse(string a)
        {
            string temp = "";
            int i, j;
            Console.WriteLine("Reversing string.");
            for (j = 0, i = a.Length - 1; i >= 0; i--, j++)
                temp += a[i];
            return temp;
        }
    }
}
