﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoLibrary
{
    public class ShoppingCartModel
    {
        // Just match the signature of the function this delegate will invoke.
        public delegate void MentionDiscount(decimal subTotal);

        public List<ProductModel> Items { get; set; } = new List<ProductModel>();

        public decimal GenerateTotal(
                       MentionDiscount mentionSubTotal,
                       Func<List<ProductModel>, decimal, decimal> calculateDiscountedTotal,
                       Action<string> discountMessage
                       )
        {
            decimal subTotal = this.Items.Sum(item => item.Price);

            mentionSubTotal(subTotal);
            discountMessage("Free Discount Pagla.");

            return calculateDiscountedTotal(Items, subTotal);

        }
    }
}
